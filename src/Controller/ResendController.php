<?php

namespace Drupal\resend_login_link_user_operation\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class DebugController.
 */
class ResendController extends ControllerBase
{
  public function resend($user, Request $request)
  {
    /** @var User $user */
    \Drupal::messenger()->addStatus('User ' . $user->getAccountName() . ' was Resent the login link.');
    _user_mail_notify('register_no_approval_required', $user);
    $previousUrl = $request->headers->get('referer');
    $response = new RedirectResponse($previousUrl);
    return $response;
  }
}
